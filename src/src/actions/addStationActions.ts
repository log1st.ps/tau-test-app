import { Dispatch } from 'redux';
import {
    ACTION_ADD_ACTIONS_SET_ERROR, ACTION_ADD_ACTIONS_SET_FOUND_STATION,
    ACTION_ADD_ACTIONS_SET_IS_LOADING,
} from '../constants/addStationActionsConstants';
import { IAddStationReducer } from '../reducers/addStation';
import { IUserReducer } from '../reducers/user';
import { getStationBySN, setStationBySN } from '../services/apiService';
import { IAction } from '../types/redux/IAction';

const emitError = (error: string | null): IAction => ({
    type: ACTION_ADD_ACTIONS_SET_ERROR,
    payload: error,
});

const setIsLoading = (value: boolean): IAction => ({
    type: ACTION_ADD_ACTIONS_SET_IS_LOADING,
    payload: value,
});

const setFoundStation = (value: IAddStationReducer['foundStation']): IAction => ({
    type: ACTION_ADD_ACTIONS_SET_FOUND_STATION,
    payload: value,
});

export const dropSearch = () => async (dispatch: Dispatch) => {
    dispatch(emitError(null));
    dispatch(setFoundStation(null));
};

export const testStationBySN = (value: string) => async (dispatch: Dispatch) => {
    dispatch(setIsLoading(true));

    const { status, data, message } = await getStationBySN(value);

    dispatch(setIsLoading(false));

    if (status !== 200 && message) {
        dispatch(emitError(message));
    }   else if (data) {
        dispatch(emitError(null));
        dispatch(setFoundStation(data));
    }
};

export const addStationBySN = (
    value: string,
) => async(
    dispatch: Dispatch, getState: (() => {user: IUserReducer}),
) => {
    dispatch(setIsLoading(true));

    await setStationBySN(value, getState().user.locationId);

    dispatch(setIsLoading(false));
    dispatch(dropSearch() as any);
};
