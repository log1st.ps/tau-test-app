import { Dispatch } from 'redux';
import {
    ACTION_USER_ADD_STATIONS,
    ACTION_USER_SET_JWT_TOKEN,
} from '../constants/userActionsConstants';
import { IUserReducer } from '../reducers/user';
import { getStations } from '../services/apiService';
import http from '../services/http';

export const addStations = (stations: IUserReducer['stations']) => ({
    type: ACTION_USER_ADD_STATIONS,
    payload: stations,
});

export const loadStations = () => async (
    dispatch: Dispatch,
    getState: (() => {user: IUserReducer}),
) => {
    const { status, data } = await getStations(getState().user.locationId);

    if (status === 200 && data) {
        dispatch(addStations(data));
    }
};

export const setJWTToken = (token: string) => (dispatch: Dispatch) => {

    dispatch(({
        type: ACTION_USER_SET_JWT_TOKEN,
        payload: token,
    }));

    // Temporary: no authorization flow, so i set it here
    http.interceptors.request.use((config: any) => ({
        ...config,
        headers: {
            ...config.headers,
            Authorization: `Bearer ${token}`,
        },
    }));
};
