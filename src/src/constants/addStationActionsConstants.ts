export const ACTION_ADD_ACTIONS_SET_IS_LOADING = 'addActions/setIsLoading';
export const ACTION_ADD_ACTIONS_SET_ERROR = 'addActions/setError';
export const ACTION_ADD_ACTIONS_SET_FOUND_STATION = 'addActions/setFoundStation';
