export interface IImageCss {
  'image': string;
  'blockType': string;
  'initialBackgroundType': string;
  'coverBackgroundType': string;
  'containBackgroundType': string;
}

export const locals: IImageCss;
