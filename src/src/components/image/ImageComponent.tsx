import cn from 'classnames';
import React, { createElement } from 'react';
import { compose, withProps } from 'recompose';
import { IImageComponent } from '../../types/components/image/IImageComponent';
import { IMAGE_TYPE_BLOCK, IMAGE_TYPE_IMG } from '../../types/components/image/imageTypesConstants';

const styles = require('./image.css');

interface IConnectedImageComponent extends IImageComponent {
    tag: string;
}

const ImageComponent = ({
    tag,
    type,
    src,
    backgroundType,
    height,
    width,
}: IConnectedImageComponent) => (
    createElement(tag, {
        src: type === IMAGE_TYPE_IMG ? src : undefined,
        style: {
            height,
            width,
            ...(type === IMAGE_TYPE_BLOCK ? {
                backgroundImage: `url(${src})`,
            } : {}),
        },
        className: cn(
            styles.image,
            styles[`${type}Type`],
            {
                [styles[`${backgroundType}BackgroundType`]]: type === IMAGE_TYPE_BLOCK,
            },
        ),
    })
);

const ImageComponentWrapped = compose<any, any>(
    withProps(({ type }: IImageComponent) => ({
        tag: type ? {
            [IMAGE_TYPE_BLOCK]: 'div',
            [IMAGE_TYPE_IMG]: 'img',
        }[type] : IMAGE_TYPE_IMG,
    })),
)(ImageComponent);

ImageComponentWrapped.defaultProps = {
    type: IMAGE_TYPE_IMG,
};

export default (props: IImageComponent) => <ImageComponentWrapped {...props}/>;
