import cn from 'classnames';
import React, { createElement } from 'react';
import { ITypographyComponent } from '../../types/components/typograpy/ITypographyComponent';
import { ITypographyCss } from './typography.css';

const styles: ITypographyCss | any = require('./typography.css');

const getTypographyComponent = (type: string) => ({
    text,
    children,
    className,
}: ITypographyComponent) => (

    <div
        className={cn(
            styles.typography,
            styles[type],
            ...(className || []),
        )}
    >
        {text}
        {children && (
            <div className={styles.append}>
                {children}
            </div>
        )}
    </div>
);

export const PrimaryHeading = (props: ITypographyComponent) => createElement(
    getTypographyComponent('primary'),
    props,
);

export const SecondaryHeading = (props: ITypographyComponent) => createElement(
    getTypographyComponent('secondary'),
    props,
);

export const TernaryHeading = (props: ITypographyComponent) => createElement(
    getTypographyComponent('ternary'),
    props,
);

interface ITextComponent extends ITypographyComponent {
    isSecondary?: boolean;
}

export const Text = ({ isSecondary, ...props }: ITextComponent) => createElement(
    getTypographyComponent('text'),
    {
        ...props,
        className: isSecondary ? [styles.isSecondary] : undefined,
    } as any,
);
