export interface ITypographyCss {
  'typography': string;
  'primary': string;
  'secondary': string;
  'ternary': string;
  'text': string;
  'isSecondary': string;
  'append': string;
}

export const locals: ITypographyCss;
