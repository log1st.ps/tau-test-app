export interface IStationsListLayoutCss {
  'layout': string;
  'title': string;
  'content': string;
}

export const locals: IStationsListLayoutCss;
