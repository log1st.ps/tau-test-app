import React from 'react';
import {
    IStationsListLayoutComponent,
} from '../../../types/components/layouts/stationsList/IStationsListLayoutComponent';

const styles = require('./stationsListLayout.css');

const StationsListLayoutComponent = ({
    renderTitle,
    children,
}: IStationsListLayoutComponent) => (
    <div className={styles.layout}>
        {renderTitle && (
            <div className={styles.title}>
                {renderTitle}
            </div>
        )}
        {children && (
            <div className={styles.content}>
                {children}
            </div>
        )}
    </div>
);

export default StationsListLayoutComponent;
