import React from 'react';

import {
    IMainLayoutComponent,
} from '../../../types/components/layouts/main/IMainLayoutComponent';
import { IMainLayoutCss } from './mainLayout.css';

const styles: IMainLayoutCss = require('./mainLayout.css');

const MainLayoutComponent = ({
    children,
    renderHeader,
    renderSide,
}: IMainLayoutComponent) => (
    <div className={styles.layout}>
        {renderHeader && (
            <div className={styles.header}>
                {renderHeader}
            </div>
        )}
        {(children || renderSide) && (
            <div className={styles.content}>
                <div className={styles.main}>
                    <div className={styles.mainContent}>
                        {children}
                    </div>
                </div>
                {renderSide && (
                    <div className={styles.side}>
                        <div className={styles.sideContent}>
                            {renderSide}
                        </div>
                    </div>
                )}
            </div>
        )}
    </div>
);

export default MainLayoutComponent;
