export interface IMainLayoutCss {
  'layout': string;
  'header': string;
  'content': string;
  'main': string;
  'mainContent': string;
  'side': string;
  'sideContent': string;
}

export const locals: IMainLayoutCss;
