import React from 'react';
import {
    IAddStationLayoutComponent,
} from '../../../types/components/layouts/addStation/IAddStationLayoutComponent';

const styles = require('./addStationLayout.css');

const AddStationLayoutComponent = ({
    renderInfo,
    renderInput,
    renderStationImage,
    renderSubmit,
    renderTitle,
    renderDescription,
}: IAddStationLayoutComponent) => (
    <div className={styles.layout}>
        {renderTitle && (
            <div className={styles.title}>
                {renderTitle}
            </div>
        )}
        {renderInfo && (
            <div className={styles.info}>
                {renderInfo}
            </div>
        )}
        {renderInput && (
            <div className={styles.input}>
                {renderInput}
            </div>
        )}
        {renderStationImage && (
            <div className={styles.stationImage}>
                {renderStationImage}
            </div>
        )}
        {renderDescription && (
            <div className={styles.description}>
                {renderDescription}
            </div>
        )}
        {renderSubmit && (
            <div className={styles.submit}>
                {renderSubmit}
            </div>
        )}
    </div>
);

export default AddStationLayoutComponent;
