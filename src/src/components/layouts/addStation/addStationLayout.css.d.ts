export interface IAddStationLayoutCss {
  'layout': string;
  'title': string;
  'info': string;
  'input': string;
  'stationImage': string;
  'description': string;
  'submit': string;
}

export const locals: IAddStationLayoutCss;
