import React, { ComponentClass, FormEvent } from 'react';
import { compose, withProps } from 'recompose';
import { IFormComponent } from '../../types/components/form/IFormComponent';
import InputElement from './elements/InputElement';

const styles = require('./form.css');

const FormComponent: ComponentClass<IFormComponent>  = compose<any, any>(
    withProps(({ onSubmit }: IFormComponent) => ({
        onSubmit: (e: FormEvent) => {
            if (onSubmit) {
                e.preventDefault();
                onSubmit();
            }
        },
    })),
)(
    ({
         children,
         onSubmit,
    }: IFormComponent) => (
        <form
            className={styles.form}
            onSubmit={onSubmit}
        >
            {children}
        </form>
    ),
);

export { InputElement };

export default (props: IFormComponent) => <FormComponent {...props}/>;
