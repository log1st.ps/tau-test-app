import cn from 'classnames';
import React, { ChangeEvent, ChangeEventHandler } from 'react';
import { compose, withProps } from 'recompose';
import { IInputElement } from '../../../types/components/form/elements/IInputElement';

const styles = require('./input.css');

const InputElement = compose<any, any>(
    withProps(({
        onChange,
    }: IInputElement) => ({
        onChange: (e: ChangeEvent<HTMLInputElement>) => {
            onChange(e.target.value);
        },
    })),
)(
    ({
         placeholder,
         state,
         renderAfter,
         value,
         onChange,
         isDisabled,
     }: IInputElement) => (
        <div
            className={cn(
                styles.field, {
                    [styles[`${state}State`]]: !!state,
                    [styles.isDisabled]: isDisabled,
                },
            )}
        >
            <input
                type={'text'}
                onChange={onChange}
                value={value}
                placeholder={placeholder}
                disabled={isDisabled}
                className={cn(
                    styles.input,
                )}
            />
            {renderAfter && (
                <div className={styles.append}>
                    {renderAfter}
                </div>
            )}
        </div>
    ),
);

export default (props: IInputElement) => <InputElement {...props}/>;
