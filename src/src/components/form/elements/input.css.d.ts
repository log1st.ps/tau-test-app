export interface IInputCss {
  'field': string;
  'errorState': string;
  'successState': string;
  'input': string;
  'isDisabled': string;
  'append': string;
}

export const locals: IInputCss;
