import * as React from 'react';
import ButtonElement from '../../elements/button/ButtonElement';
import { IActionsListComponent } from '../../types/components/actionsList/IActionsListComponent';

const styles = require('./actionsList.css');

const ActionsListComponent = ({
    actions,
    buttonsPerRow,
}: IActionsListComponent) => (
    <div
        className={styles.container}
        style={{
            gridTemplateColumns:
              (buttonsPerRow && buttonsPerRow > 1)
                  ? Array(buttonsPerRow).fill(null).map(() => '1fr').join(' ')
                  : '1fr',
        }}
    >
        {actions && actions.map(({ key, ...button }) => (
            <div key={key}>
                <ButtonElement {...button}/>
            </div>
        ))}
    </div>
);

ActionsListComponent.defaultProps = {
    buttonsPerRow: 1,
};

export default ActionsListComponent;
