import React from 'react';
import { compose } from 'recompose';
import ButtonElement from '../../elements/button/ButtonElement';
import IconElement from '../../elements/icon/IconElement';
import LogoElement from '../../elements/logo/LogoElement';
import { IHeaderComponent } from '../../types/components/header/IHeaderComponent';
import { BUTTON_SIZE_MD } from '../../types/elements/button/buttonSizesConstants';
import { BUTTON_STATE_INITIAL_OPACITY_70 } from '../../types/elements/button/buttonStatesConstants';
import { ICON_SIZE_STRETCH } from '../../types/elements/icon/iconSizesConstants';
import { ICON_STATE_COLOR_INHERIT } from '../../types/elements/icon/iconStatesConstants';
import { ICON_HAMBURGER } from '../../types/elements/icon/iconTypesConstants';
const styles = require('./header.css');

const HeaderComponent = ({
    onLogoClick,
    onHamburgerClick,
}: IHeaderComponent) => (
    <div className={styles.header}>
        <div className={styles.container}>
            <div className={styles.hamburger}>
                <ButtonElement
                    onClick={onHamburgerClick}
                    size={BUTTON_SIZE_MD}
                    state={BUTTON_STATE_INITIAL_OPACITY_70}
                    isSquare={true}
                >
                    <IconElement
                        type={ICON_HAMBURGER}
                        size={ICON_SIZE_STRETCH}
                        state={ICON_STATE_COLOR_INHERIT}
                    />
                </ButtonElement>
            </div>
            <div className={styles.logo}>
                <ButtonElement onClick={onLogoClick}>
                    <LogoElement/>
                </ButtonElement>
            </div>
        </div>
    </div>
);

const HeaderComponentWrapped = compose(

)(HeaderComponent as any);

export default (props: IHeaderComponent) => <HeaderComponentWrapped {...props}/>;
