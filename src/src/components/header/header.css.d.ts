export interface IHeaderCss {
  'header': string;
  'container': string;
  'hamburger': string;
  'logo': string;
}

export const locals: IHeaderCss;
