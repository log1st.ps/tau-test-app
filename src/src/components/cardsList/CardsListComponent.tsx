import React from 'react';
import { ICardsListComponent } from '../../types/components/cardsList/ICardsListComponent';
import CardComponent from '../card/CardComponent';

const styles = require('./cardsList.css');

const CardsListComponent = ({
    cardsPerRow,
    cards,
}: ICardsListComponent) => (
    <div
        className={styles.container}
        style={{
            gridTemplateColumns:
                (cardsPerRow && cardsPerRow > 1)
                    ? Array(cardsPerRow).fill(null).map(() => '1fr').join(' ')
                    : '1fr',
        }}
    >
        {cards.map(({ key, ...card }) => (
            <div
                key={key}
                className={styles.card}
            >
                <CardComponent {...card}/>
            </div>
        ))}
    </div>
);

export default CardsListComponent;
