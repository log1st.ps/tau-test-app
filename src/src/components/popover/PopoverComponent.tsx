import cn from 'classnames';
import React from 'react';
import { IPopoverComponent } from '../../types/components/popover/IPopoverComponent';

const styles = require('./popover.css');

const PopoverComponent = ({
    iconPosition,
    children,
    onBlur,
}: IPopoverComponent) => (
    <div
        onMouseLeave={onBlur}
        className={styles.popover}
    >
        <div className={styles.container}>
            {iconPosition && (
                <div
                    className={cn(
                        styles.icon,
                        styles[`${iconPosition}IconPosition`],
                    )}
                />
            )}
            <div className={styles.content}>
                {children}
            </div>
        </div>
    </div>
);

export default PopoverComponent;
