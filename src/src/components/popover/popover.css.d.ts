export interface IPopoverCss {
  'popover': string;
  'container': string;
  'icon': string;
  'topLeftIconPosition': string;
  'content': string;
}

export const locals: IPopoverCss;
