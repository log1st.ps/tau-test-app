import cn from 'classnames';
import React from 'react';
import { IPositionBlock, IRelativeBlock } from '../../types/components/helpers/IHelpersComponent';

const styles = require('./helpers.css');

const getPositionBlock = (type: string) => ({
    children,
    top,
    right,
    bottom,
    left,
}: IPositionBlock) => (
    <div
        className={cn(
            styles[type],
        )}
        style={{
            top,
            right,
            bottom,
            left,
        }}
    >
        {children}
    </div>
);

export const RelativeBlock = getPositionBlock('relative');

export const AbsoluteBlock = getPositionBlock('absolute');
