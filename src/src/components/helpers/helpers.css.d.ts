export interface IHelpersCss {
  'relative': string;
  'absolute': string;
}

export const locals: IHelpersCss;
