export interface ICardCss {
  'card': string;
  'image': string;
  'gridInfo': string;
  'infoItem': string;
  'infoItemLabel': string;
  'infoItemValue': string;
}

export const locals: ICardCss;
