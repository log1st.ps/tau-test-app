import React, { ReactNode } from 'react';
import { ICardComponent } from '../../types/components/card/ICardComponent';
import { IMAGE_BACKGROUND_TYPE_CONTAIN } from '../../types/components/image/imageBackgroundTypesConstants';
import { IMAGE_TYPE_BLOCK } from '../../types/components/image/imageTypesConstants';
import ImageComponent from '../image/ImageComponent';
import { TernaryHeading, Text } from '../typography/TypographyComponent';

const styles = require('./card.css');

const CardComponent = ({
    title,
    imageUrl,
    gridInfo,
}: ICardComponent) => (
    <div className={styles.card}>
        {title && (
            <div className={styles.title}>
                <TernaryHeading text={title}/>
            </div>
        )}
        {imageUrl && (
            <div
                className={styles.image}
            >
                <ImageComponent
                    src={imageUrl}
                    type={IMAGE_TYPE_BLOCK}
                    height={112}
                    width={'100%'}
                    backgroundType={IMAGE_BACKGROUND_TYPE_CONTAIN}
                />
            </div>
        )}
        {gridInfo && (
            <div className={styles.gridInfo}>
                {gridInfo.map(({ label, value }) => (
                    <div
                        key={label}
                        className={styles.infoItem}
                    >
                        <div className={styles.infoItemLabel}>
                            <Text text={label}/>
                        </div>
                        {value && (
                            <div className={styles.infoItemValue}>
                                {typeof value === 'string' ? (
                                    <Text isSecondary={true} text={value}/>
                                ) : value}
                            </div>
                        )}
                    </div>
                ))}
            </div>
        )}
    </div>
);

export default CardComponent;
