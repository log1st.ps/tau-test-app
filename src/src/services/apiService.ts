import { IAddStationReducer } from '../reducers/addStation';
import { IUserReducer } from '../reducers/user';
import http from './http';

interface IApiResponse {
    status: number;
    message?: string;
}

interface IApiStationResponse extends IApiResponse {
    data?: IAddStationReducer['foundStation'];
}

interface IApiStationsResponse extends IApiResponse {
    data?: IUserReducer['stations'];
}

const getApiUrl = (
    endpoint: string,
) => `${process.env.API_URL || 'https://api.tau.green/v1'}${endpoint}`;

export const getStationBySN = async (number: string): Promise<IApiStationResponse> => {

    const {
        data:
            {
                id,
                model,
                image,
            },
    } = await http.get(getApiUrl(`/stations/${number}`));

    if (id) {
        return {
            status: 200,
            data: {
                name: model,
                imageUrl: image,
            },
        };
    }

    return { status: 400, message: 'Invalid serial' };
};

export const setStationBySN = async (
    number: string, location: string,
): Promise<IApiStationResponse> => {
    const { data } = await http.post(
        getApiUrl(`/locations/${location}/stations`),
        {
            serial_number: number,
        },
    );

    // Strange response
    if (data === 'OK') {
        return {
            status: 200,
        };
    }

    return {
        status: 400,
        message: 'Unable to add station',
    };
};

export const getStations = async (location: string): Promise<IApiStationsResponse> => {

    const { data }: {data: Array<{[key: string]: string}>} = await http.get(
        `https://api.tau.green/v1/locations/${location}/stations`,
    );

    if (Array.isArray(data)) {
        return {
            status: 200,
            data: data.map(
                ({
                    id,
                    model,
                    image,
                }) => ({
                    id,
                    name: model,
                    imageUrl: image,

                    // What fields from response related to next three attributes?
                    type: 'electric',
                    current: 'AC',
                    power: '22 kw',
                }),
            ),
        };
    }

    return {
        status: 400,
        message: 'Unable to get stations',
    };
};
