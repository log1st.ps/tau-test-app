import { Reducer } from 'redux';
import {
    ACTION_USER_ADD_STATIONS,
    ACTION_USER_SET_JWT_TOKEN,
} from '../constants/userActionsConstants';
import { IAction } from '../types/redux/IAction';

export interface IUserReducer {
    JWTToken: string | null;
    locationId: string;
    stations: Array<{
        id: string,
        name: string,
        imageUrl: string,
        type: string,
        current: string,
        power: string,
    }>;
}

const initialState = {
    stations: [],
    JWTToken: null,
    locationId: '1d2cb870-199e-4afd-8efb-e9345cd2cdbf',
};

export default (
    state: IUserReducer = initialState,
    { type, payload }: IAction,
): Reducer<any> => (
    ({
        [ACTION_USER_ADD_STATIONS]: () => ({
            ...state,
            stations: payload,
        }),
        [ACTION_USER_SET_JWT_TOKEN]: () => ({
            ...state,
            JWTToken: payload,
        }),
    } as  any)[type]
    || (() => state)
)();
