import { Reducer } from 'redux';
import {
    ACTION_ADD_ACTIONS_SET_ERROR,
    ACTION_ADD_ACTIONS_SET_FOUND_STATION,
    ACTION_ADD_ACTIONS_SET_IS_LOADING,
} from '../constants/addStationActionsConstants';
import { IAction } from '../types/redux/IAction';

export interface IAddStationReducer {
    error: string | null;
    isLoading: boolean;
    foundStation: {
        name: string,
        imageUrl: string,
    } | null;
}

const initialState = {
    error: null,
    isLoading: false,
    foundStation: null,
};

export default (
    state: IAddStationReducer = initialState,
    { type, payload }: IAction,
): Reducer<any> => (
    ({
        [ACTION_ADD_ACTIONS_SET_ERROR]: (): IAddStationReducer => ({
            ...state,
            error: payload,
        }),
        [ACTION_ADD_ACTIONS_SET_IS_LOADING]: (): IAddStationReducer => ({
            ...state,
            isLoading: payload,
        }),
        [ACTION_ADD_ACTIONS_SET_FOUND_STATION]: (): IAddStationReducer => ({
            ...state,
            foundStation: payload,
        }),
    } as  any)[type]
    || (() => state)
)();
