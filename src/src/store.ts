import { applyMiddleware, combineReducers, compose, createStore } from 'redux';
import thunk from 'redux-thunk';
import addStation from './reducers/addStation';
import user from './reducers/user';

export default createStore(
    combineReducers(
        {
            addStation: addStation as any,
            user: user as any,
        },
    ),
    compose(
        applyMiddleware(thunk),
        ...[
            process.env.NODE_ENV === 'development'
                ? (
                    // @ts-ignore
                    window.__REDUX_DEVTOOLS_EXTENSION__
                        // @ts-ignore
                        ? window.__REDUX_DEVTOOLS_EXTENSION__()
                        // @ts-ignore
                        : f => f
                )
                : false,
        ].filter(Boolean),
    ),
);
