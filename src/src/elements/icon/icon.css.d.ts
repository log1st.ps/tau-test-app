export interface IIconCss {
  'icon': string;
  'stretchSize': string;
  'xsSize': string;
  'colorInheritState': string;
}

export const locals: IIconCss;
