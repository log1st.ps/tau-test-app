import cn from 'classnames';
import React, { ClassType, ComponentClass, createElement, FunctionComponent } from 'react';
import { compose, withProps } from 'recompose';
import { IIconElement } from '../../types/elements/icon/IIconElement';
import { IIconCss } from './icon.css';

const styles: IIconCss | any = require('./icon.css');

const getIconSource = (type: string): any => require(`./sources/${type}.inline.svg`);

// tslint:disable-next-line:no-empty-interface
interface IIconElementWrapped extends IIconElement {
    source: ComponentClass;
}

const IconElement = ({
    source,
    size,
    state,
}: IIconElementWrapped) => (
    createElement(source, {
        className: cn(
          styles.icon, {
              [styles[`${size}Size`]]: !!size,
              [styles[`${state}State`]]: !!state,
          },
        ),
    } as any)
);

const IconElementWrapped = compose(
    withProps(
        ({ type }: IIconElement) => ({
            source: getIconSource(type),
        }),
    ),
)(IconElement as any);

export default (props: IIconElement) => <IconElementWrapped {...props}/>;
