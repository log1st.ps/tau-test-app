import * as React from 'react';
import { ICON_SIZE_STRETCH } from '../../types/elements/icon/iconSizesConstants';
import { ICON_STATE_COLOR_INHERIT } from '../../types/elements/icon/iconStatesConstants';
import { ICON_LOGO } from '../../types/elements/icon/iconTypesConstants';
import IconElement from '../icon/IconElement';

const styles = require('./logo.css');

const LogoElement = () => (
    <div className={styles.logo}>
        <IconElement
            type={ICON_LOGO}
            state={ICON_STATE_COLOR_INHERIT}
            size={ICON_SIZE_STRETCH}
        />
    </div>
);

export default LogoElement;
