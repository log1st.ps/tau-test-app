export interface IButtonCss {
  'button': string;
  'isBlock': string;
  'content': string;
  'isDisabled': string;
  'xsSize': string;
  'isSquare': string;
  'mdSize': string;
  'xlSize': string;
  'initialOpacity70State': string;
  'primaryColorState': string;
  'primaryState': string;
}

export const locals: IButtonCss;
