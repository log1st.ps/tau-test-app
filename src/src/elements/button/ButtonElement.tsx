import cn from 'classnames';
import React, { createElement } from 'react';
import { compose, withProps } from 'recompose';
import { IButtonElement } from '../../types/elements/button/IButtonElement';
import { IButtonCss } from './button.css';

const styles: IButtonCss | any = require('./button.css');

interface IButtonElementWrapped extends IButtonElement {
    tag: string;
    stateClasses?: string[];
}

const ButtonElement = ({
    tag,
    children,
    size,
    isSquare,
    stateClasses,
    isDisabled,
    onClick,
    onHover,
    onBlur,
    isBlock,
}: IButtonElementWrapped) => (
    createElement(
        tag as string, {
            onClick,
            disabled: isDisabled,
            onMouseEnter: onHover,
            onMouseLeave: onBlur,
            className: cn(
              styles.button, {
                  [styles[`${size}Size`]]: !!size,
                  [styles.isSquare]: isSquare,
                  [styles.isBlock]: isBlock,
                  [styles.isDisabled]: isDisabled,
              },
              stateClasses,
          ),
        } as any,
        (
            <div className={styles.content}>
                {children}
            </div>
        ),
    )
);

const ButtonElementWrapped =  compose(
    withProps({
        tag: 'button',
    }),
    withProps(({ state }: IButtonElement) => ({
        stateClasses: state ? (Array.isArray(state) ? state : [state]).map(
            stateItem => styles[`${stateItem}State`],
        ) : [],
    })),
)(ButtonElement as any);

export default (props: IButtonElement) => <ButtonElementWrapped {...props}/>;
