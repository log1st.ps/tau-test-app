import { IButtonElement } from '../../elements/button/IButtonElement';

export interface IConnectedButtonElement extends IButtonElement {
    key: string;
}

export interface IActionsListComponent {
    actions?: IConnectedButtonElement[];
    buttonsPerRow?: number;
}
