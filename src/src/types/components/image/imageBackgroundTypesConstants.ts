export const IMAGE_BACKGROUND_TYPE_INITIAL = 'initial';
export const IMAGE_BACKGROUND_TYPE_COVER = 'cover';
export const IMAGE_BACKGROUND_TYPE_CONTAIN = 'contain';
