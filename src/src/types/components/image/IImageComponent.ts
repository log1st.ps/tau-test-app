import {
    IMAGE_BACKGROUND_TYPE_CONTAIN,
    IMAGE_BACKGROUND_TYPE_COVER,
    IMAGE_BACKGROUND_TYPE_INITIAL,
} from './imageBackgroundTypesConstants';
import { IMAGE_TYPE_BLOCK, IMAGE_TYPE_IMG } from './imageTypesConstants';

export interface IImageComponent {
    type?:
        typeof IMAGE_TYPE_BLOCK
        | typeof IMAGE_TYPE_IMG;
    src: string;
    height?: number | string;
    width?: number | string;
    backgroundType?:
        typeof IMAGE_BACKGROUND_TYPE_INITIAL
        | typeof IMAGE_BACKGROUND_TYPE_CONTAIN
        | typeof IMAGE_BACKGROUND_TYPE_COVER;
}
