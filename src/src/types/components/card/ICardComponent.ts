import { ReactNode } from 'react';

export interface ICardComponent {
    title?: string;
    imageUrl?: string;
    gridInfo?: Array<{
        label: string,
        value: string | ReactNode,
    }>;
}
