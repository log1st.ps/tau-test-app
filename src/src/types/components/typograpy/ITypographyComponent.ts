import { ReactNode } from 'react';

export interface ITypographyComponent {
    text?: string;
    children?: ReactNode;
    className?: [];
}
