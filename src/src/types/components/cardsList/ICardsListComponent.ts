import { ICardComponent } from '../card/ICardComponent';

export interface IConnectedCardComponent extends ICardComponent {
    key: string;
}

export interface ICardsListComponent {
    cards: IConnectedCardComponent[];
    cardsPerRow: number;
}
