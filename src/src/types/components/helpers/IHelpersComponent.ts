import { ReactNode } from 'react';

export interface IPositionBlock {
    left?: number;
    top?: number;
    right?: number;
    bottom?: number;
    children?: ReactNode;
}

// tslint:disable-next-line:no-empty-interface
export interface IRelativeBlock extends IPositionBlock {
}

// tslint:disable-next-line:no-empty-interface
export interface IAbsoluteBlock extends IPositionBlock {
}
