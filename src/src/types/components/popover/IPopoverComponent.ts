import { ReactNode } from 'react';
import {
    POPOVER_ICON_POSITION_BOTTOM_LEFT,
    POPOVER_ICON_POSITION_BOTTOM_RIGHT,
    POPOVER_ICON_POSITION_TOP_LEFT, POPOVER_ICON_POSITION_TOP_RIGHT,
} from './popoverIconPositionsConstants';

export interface IPopoverComponent {
    children?: ReactNode;
    iconPosition?:
      typeof POPOVER_ICON_POSITION_TOP_LEFT
      | typeof POPOVER_ICON_POSITION_TOP_RIGHT
      | typeof POPOVER_ICON_POSITION_BOTTOM_RIGHT
      | typeof POPOVER_ICON_POSITION_BOTTOM_LEFT;
    onBlur?(): void;
}
