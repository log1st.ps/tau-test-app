export const POPOVER_ICON_POSITION_TOP_LEFT = 'topLeft';
export const POPOVER_ICON_POSITION_TOP_RIGHT = 'topRight';
export const POPOVER_ICON_POSITION_BOTTOM_RIGHT = 'bottomRight';
export const POPOVER_ICON_POSITION_BOTTOM_LEFT = 'bottomLeft';
