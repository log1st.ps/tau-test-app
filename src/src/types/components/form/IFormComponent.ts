import { ReactNode } from 'react';
import { FORM_FIELD_STATE_ERROR, FORM_FIELD_STATE_SUCCESS } from './formFieldStatesConstants';

export type availableFieldStates =
  typeof FORM_FIELD_STATE_ERROR
  | typeof FORM_FIELD_STATE_SUCCESS;

export interface IFormField {
    placeholder?: string;
    state?: availableFieldStates;
    renderAfter?: ReactNode;
    value?: string;
    onChange?: ((value: string) => void) | any;
    isDisabled?: boolean;
}

export interface IFormComponent {
    children?: ReactNode;
    onSubmit?(): void;
}
