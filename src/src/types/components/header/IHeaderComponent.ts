export interface IHeaderComponent {
    onHamburgerClick?(): void;
    onLogoClick?(): void;
}
