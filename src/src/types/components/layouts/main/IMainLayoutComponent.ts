import { ReactNode } from 'react';

export interface IMainLayoutComponent {
    children?: ReactNode;
    renderHeader?: ReactNode;

    renderSide?: ReactNode;
}
