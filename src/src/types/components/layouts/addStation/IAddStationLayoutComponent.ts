import { ReactNode } from 'react';

export interface IAddStationLayoutComponent {
    renderTitle?: ReactNode;
    renderInfo?: ReactNode;
    renderInput?: ReactNode;
    renderSubmit?: ReactNode;
    renderStationImage?: ReactNode;
    renderDescription?: ReactNode;
}
