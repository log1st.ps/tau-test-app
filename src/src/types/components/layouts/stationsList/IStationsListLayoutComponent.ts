import { ReactNode } from 'react';

export interface IStationsListLayoutComponent {
    children?: ReactNode;
    renderTitle?: ReactNode;
}
