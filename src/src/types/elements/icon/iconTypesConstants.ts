export const ICON_HAMBURGER = 'hamburger';
export const ICON_LOGO = 'logo';
export const ICON_POPUP_QUESTION = 'popup-question';
export const ICON_CHECK_ROUND = 'check-round';
export const ICON_ELECTRIC_TYPE = 'electric-type';
