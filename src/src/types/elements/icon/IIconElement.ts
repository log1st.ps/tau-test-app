import { ICON_SIZE_STRETCH, ICON_SIZE_XS } from './iconSizesConstants';
import { ICON_STATE_COLOR_INHERIT } from './iconStatesConstants';
import {
    ICON_CHECK_ROUND, ICON_ELECTRIC_TYPE,
    ICON_HAMBURGER,
    ICON_LOGO,
    ICON_POPUP_QUESTION,
} from './iconTypesConstants';

export interface IIconElement {
    type:
      typeof ICON_HAMBURGER
      | typeof ICON_LOGO
      | typeof ICON_POPUP_QUESTION
      | typeof ICON_CHECK_ROUND
      | typeof ICON_ELECTRIC_TYPE
  ;
    size?:
      typeof ICON_SIZE_STRETCH
      | typeof ICON_SIZE_XS
    ;
    state?:
      typeof ICON_STATE_COLOR_INHERIT;
}
