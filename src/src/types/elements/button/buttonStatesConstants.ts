export const BUTTON_STATE_INITIAL_OPACITY_70 = 'initialOpacity70';
export const BUTTON_STATE_PRIMARY = 'primary';
export const BUTTON_STATE_PRIMARY_COLOR = 'primaryColor';
