import { ReactNode } from 'react';
import { BUTTON_SIZE_MD, BUTTON_SIZE_XL, BUTTON_SIZE_XS } from './buttonSizesConstants';
import {
  BUTTON_STATE_INITIAL_OPACITY_70, BUTTON_STATE_PRIMARY,
  BUTTON_STATE_PRIMARY_COLOR,
} from './buttonStatesConstants';

type availableStates =
    typeof BUTTON_STATE_INITIAL_OPACITY_70
    | typeof BUTTON_STATE_PRIMARY
    | typeof BUTTON_STATE_PRIMARY_COLOR;

export interface IButtonElement {
    children?: ReactNode;
    size?:
    typeof BUTTON_SIZE_MD
    | typeof BUTTON_SIZE_XS
    | typeof BUTTON_SIZE_XL
  ;
    state?:
      availableStates
      | availableStates[];
    isSquare?: boolean;
    isBlock?: boolean;
    isDisabled?: boolean;
    onClick?(): void;
    onHover?(): void;
    onBlur?(): void;
}
