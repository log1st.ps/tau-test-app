import React from 'react';
import { connect } from 'react-redux';
import { compose, lifecycle, withProps } from 'recompose';
import { loadStations } from '../actions/userActions';
import CardsListComponent from '../components/cardsList/CardsListComponent';
import StationsListLayoutComponent from '../components/layouts/stationsList/StationsListLayoutComponent';
import { PrimaryHeading } from '../components/typography/TypographyComponent';
import IconElement from '../elements/icon/IconElement';
import { IUserReducer } from '../reducers/user';
import { IConnectedCardComponent } from '../types/components/cardsList/ICardsListComponent';
import { ICON_SIZE_XS } from '../types/elements/icon/iconSizesConstants';
import { ICON_STATE_COLOR_INHERIT } from '../types/elements/icon/iconStatesConstants';
import { ICON_ELECTRIC_TYPE } from '../types/elements/icon/iconTypesConstants';

interface IStationsListContainer {
    heading: string;
    stations: IUserReducer['stations'];
    cards: IConnectedCardComponent[];
    triggerLoadStations(): void;
}

const StationsListContainer = compose<any, any>(
    connect(
        (store: {user: IUserReducer}) => ({
            stations: store.user.stations,
        }),
        {
            triggerLoadStations: loadStations,
        },
    ),
    withProps({
        heading: 'Your Charging Stations',
    }),
    lifecycle({
        componentDidMount() {
            const {
                triggerLoadStations,
            } = this.props as IStationsListContainer;

            triggerLoadStations();
        },
    }),
    withProps(({ stations }: IStationsListContainer) => ({
        cards: stations.map(({
            id,
            name,
            imageUrl,
            current,
            type,
            power,
        }): IConnectedCardComponent => ({
            imageUrl,
            key: id,
            title: name,
            gridInfo: [
                { label: 'Type', value: (({
                    electric: (
                        <IconElement
                            state={ICON_STATE_COLOR_INHERIT}
                            type={ICON_ELECTRIC_TYPE}
                            size={ICON_SIZE_XS}
                        />
                    ),
                } as any)[type] || type) },
                { label: 'Current', value: current },
                { label: 'Power', value: power },
            ],
        })),
    })),
)(
    ({
        heading,
        cards,
    }: IStationsListContainer) => (
         <StationsListLayoutComponent
             renderTitle={<PrimaryHeading text={heading}/>}
         >
             <CardsListComponent cards={cards} cardsPerRow={2}/>
         </StationsListLayoutComponent>
    ),
);

export default StationsListContainer;
