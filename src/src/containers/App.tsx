import React from 'react';
import { connect } from 'react-redux';
import { compose, lifecycle, withProps } from 'recompose';
import { setJWTToken } from '../actions/userActions';
import HeaderComponent from '../components/header/HeaderComponent';
import MainLayoutComponent from '../components/layouts/main/MainLayoutComponent';
import { IHeaderComponent } from '../types/components/header/IHeaderComponent';
import AddStationContainer from './addStation/AddStationContainer';
import StationsListContainer from './StationsListContainer';

const Header = compose(
    connect(
        null,
        {
            setToken: setJWTToken,
        },
    ),
    lifecycle({
        componentDidMount() {
            const {
                setToken,
            } = this.props as any;

            // Temporary: no authorization flow, so i set it here
            setToken('eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImI1OTA5OWQ5LWY2MjItNGU1Yy05YWE4LTgyY2ZiYTk4OGY4YSIsImVtYWlsIjoidWNhQG9idXJhcHVyLnNyIiwicGhvbmUiOiIwOTEzMzc5MTk4IiwiaWF0IjoxNTU1NjIwMjc2LCJleHAiOjE1NjA4MDQyNzZ9.sLqSfE8ncCrIoEonWqCX7OC_2ObX58pO4F6ZhNmR_iA');
        },
    }),
)(HeaderComponent as any);

const App = () => (
    <MainLayoutComponent
        renderHeader={<Header/>}
        renderSide={<StationsListContainer/>}
    >
        <AddStationContainer/>
    </MainLayoutComponent>
);

export default App;
