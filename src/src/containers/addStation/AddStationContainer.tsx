import React, { Fragment, ReactNode } from 'react';
import { connect } from 'react-redux';
import { compose, withProps, withState } from 'recompose';
import { addStationBySN, dropSearch, testStationBySN } from '../../actions/addStationActions';
import ActionsListComponent from '../../components/actionsList/ActionsListComponent';
import ImageComponent from '../../components/image/ImageComponent';
import AddStationLayoutComponent from '../../components/layouts/addStation/AddStationLayoutComponent';
import {
    PrimaryHeading, SecondaryHeading,
} from '../../components/typography/TypographyComponent';
import { IAddStationReducer } from '../../reducers/addStation';
import { IConnectedButtonElement } from '../../types/components/actionsList/IActionsListComponent';
import { IMAGE_BACKGROUND_TYPE_COVER } from '../../types/components/image/imageBackgroundTypesConstants';
import { IMAGE_TYPE_BLOCK } from '../../types/components/image/imageTypesConstants';
import { BUTTON_SIZE_XL } from '../../types/elements/button/buttonSizesConstants';
import {
    BUTTON_STATE_INITIAL_OPACITY_70,
    BUTTON_STATE_PRIMARY,
} from '../../types/elements/button/buttonStatesConstants';
import AddStationForm from './AddStationForm';
import AddStationInfoTextComponent from './AddStationInfoTextComponent';

interface IAddStationContainer {
    texts: {
        heading: string,
    };
    children?: ReactNode;

    value: string;

    isLoading: boolean;

    foundStation: IAddStationReducer['foundStation'];

    description?: string;

    actions: IConnectedButtonElement[];
    setValue(value: string): void;

    onSubmitSN(value: string): void;
    onSubmit(): void;
    onAddSN(value: string): void;
    onAdd(): void;
    onDropSearch(): void;
}

const AddStationContainer = compose(
    connect(
        (state: {addStation: IAddStationReducer}) => ({
            isLoading: state.addStation.isLoading,
            foundStation: state.addStation.foundStation,
        }),
        {
            onSubmitSN: testStationBySN,
            onDropSearch: dropSearch,
            onAddSN: addStationBySN,
        },
    ),
    withProps({
        texts: {
            heading: 'Add Charging Station',
            info: 'Enter serial number of your charging station.',
        },
    }),
    withState('value', 'setValue', ''),
    withProps(({
        value,
        setValue,
        onDropSearch,
        onAddSN,
        onSubmitSN,
    }: IAddStationContainer) => ({
        setValue(val: string) {
            onDropSearch();
            setValue(val);
        },
        onSubmit() {
            onSubmitSN(value);
        },
        onAdd() {
            onAddSN(value);
            setValue('');
        },
    })),
    withProps(({
        onSubmit,
        isLoading,
        value,
        foundStation,
        onAdd,
    }: IAddStationContainer): {
        actions: IConnectedButtonElement[],
    } => ({
        actions: [{
            key: 'string',
            children: foundStation ? 'Add Station' : 'NEXT',
            state: [BUTTON_STATE_PRIMARY, BUTTON_STATE_INITIAL_OPACITY_70],
            size: BUTTON_SIZE_XL,
            isBlock: true,
            onClick: foundStation ? onAdd : onSubmit,
            isDisabled: isLoading || !value,
        }],
    })),
    withProps(({ foundStation }: IAddStationContainer) => (foundStation ? {
        description: `Your charging station is ${foundStation.name}`,
    } : {})),
)(
    ({
        texts: {
            heading,
            info,
        },
        value,
        setValue,
        actions,
        description,
        foundStation,
    }: IAddStationContainer | any) => (
        <Fragment>
            <AddStationLayoutComponent
                renderTitle={<PrimaryHeading text={heading}/>}
                renderInfo={<AddStationInfoTextComponent text={info}/>}
                renderInput={<AddStationForm value={value} setValue={setValue}/>}
                renderStationImage={foundStation && (
                    <ImageComponent
                        src={foundStation.imageUrl}
                        width={168}
                        height={194}
                        type={IMAGE_TYPE_BLOCK}
                        backgroundType={IMAGE_BACKGROUND_TYPE_COVER}
                    />
                )}
                renderDescription={foundStation && (
                    <SecondaryHeading text={description} />
                )}
                renderSubmit={<ActionsListComponent actions={actions}/>}
            />
        </Fragment>
    ),
);

export default AddStationContainer;
