import React from 'react';
import { compose, withProps, withState } from 'recompose';
import { AbsoluteBlock, RelativeBlock } from '../../components/helpers/HelpersComponent';
import PopoverComponent from '../../components/popover/PopoverComponent';
import { Text } from '../../components/typography/TypographyComponent';
import ButtonElement from '../../elements/button/ButtonElement';
import IconElement from '../../elements/icon/IconElement';
import {
    POPOVER_ICON_POSITION_TOP_LEFT,
} from '../../types/components/popover/popoverIconPositionsConstants';
import { BUTTON_SIZE_XS } from '../../types/elements/button/buttonSizesConstants';
import { BUTTON_STATE_PRIMARY_COLOR } from '../../types/elements/button/buttonStatesConstants';
import { ICON_SIZE_STRETCH } from '../../types/elements/icon/iconSizesConstants';
import { ICON_STATE_COLOR_INHERIT } from '../../types/elements/icon/iconStatesConstants';
import { ICON_POPUP_QUESTION } from '../../types/elements/icon/iconTypesConstants';

interface IInfoText {
    text: string;
    popoverText: string;
    isPopupActive: boolean;
    setIsPopupActive(value: boolean): () => void;
    onHover(): void;
    onBlur(): void;
}

export default compose<any, any>(
    withProps({
        popoverText: 'You can find serial number on your invoice ' +
            'or on the right side of your charging station.',
    }),
    withState('isPopupActive', 'setIsPopupActive', false),
    withProps(({
                   setIsPopupActive,
    }: IInfoText) => ({
        onHover: () => setIsPopupActive(true),
        onBlur: () => setIsPopupActive(false),
    })),
)(
    ({
         text,
         isPopupActive,
         onBlur,
         onHover,
         popoverText,
     }: IInfoText) => (
        <Text text={text}>
            <RelativeBlock>
                <ButtonElement
                    size={BUTTON_SIZE_XS}
                    state={[BUTTON_STATE_PRIMARY_COLOR]}
                    isSquare={true}
                    onHover={onHover}
                >
                    <IconElement
                        type={ICON_POPUP_QUESTION}
                        state={ICON_STATE_COLOR_INHERIT}
                        size={ICON_SIZE_STRETCH}
                    />
                </ButtonElement>
                {isPopupActive && (
                    <AbsoluteBlock
                        top={0}
                        left={0}
                    >
                        <PopoverComponent
                            iconPosition={POPOVER_ICON_POSITION_TOP_LEFT}
                            onBlur={onBlur}
                        >
                            {popoverText}
                        </PopoverComponent>
                    </AbsoluteBlock>
                )}
            </RelativeBlock>
        </Text>
    ),
);
