import * as React from 'react';
import { FormEvent, FormEventHandler } from 'react';
import { connect } from 'react-redux';
import { compose, withProps, withState } from 'recompose';
import { testStationBySN } from '../../actions/addStationActions';
import FormComponent, { InputElement } from '../../components/form/FormComponent';
import IconElement from '../../elements/icon/IconElement';
import { IAddStationReducer } from '../../reducers/addStation';
import {
    FORM_FIELD_STATE_ERROR,
    FORM_FIELD_STATE_SUCCESS,
} from '../../types/components/form/formFieldStatesConstants';
import { ICON_SIZE_XS } from '../../types/elements/icon/iconSizesConstants';
import { ICON_STATE_COLOR_INHERIT } from '../../types/elements/icon/iconStatesConstants';
import { ICON_CHECK_ROUND } from '../../types/elements/icon/iconTypesConstants';

export interface IAddStationForm {
    value: string;
    setValue(value: string): void;
}

interface IConnectedAddStationForm extends IAddStationForm {
    texts: {
        placeholder: string,
    };
    error?: string;
    isLoading: boolean;
    isStationFound: boolean;
    onSubmit?(): void;
    onFormSubmit?(value: string): void;
}

const AddStationForm: React.ComponentClass<IAddStationForm> = compose<any, any>(
    connect(
        (state: {
            addStation: IAddStationReducer,
        }) => ({
            error: state.addStation.error,
            isLoading: state.addStation.isLoading,
            isStationFound: !!state.addStation.foundStation,
        }),
        {
            onFormSubmit: testStationBySN,
        },
    ),
    withProps({
        texts: {
            placeholder: 'e.g. 02341932',
        },
    }),
    withProps(({ value, onFormSubmit }: IConnectedAddStationForm) => ({
        onSubmit() {
            if (onFormSubmit) {
                onFormSubmit(value);
            }
        },
    })),
)(
    ({
        value,
        setValue,
        error,
        isLoading,
         isStationFound,
        onSubmit,
        texts: {
            placeholder,
        },
    }: IConnectedAddStationForm) => (
        <FormComponent
            onSubmit={onSubmit}
        >
            <InputElement
                value={value}
                onChange={setValue}
                placeholder={error || placeholder}
                state={
                    error
                        ? FORM_FIELD_STATE_ERROR
                        : (isStationFound ? FORM_FIELD_STATE_SUCCESS : undefined)
                }
                isDisabled={isLoading}
                renderAfter={
                    isStationFound
                        ? (
                            <IconElement
                                type={ICON_CHECK_ROUND}
                                state={ICON_STATE_COLOR_INHERIT}
                                size={ICON_SIZE_XS}
                            />
                        )
                        : undefined
                }
            />
        </FormComponent>
    ),
);

export default AddStationForm;
